

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Forward")
public class Forward extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int value = Integer.parseInt(request.getParameter("input"));
		
		if(value<10)
		{
			RequestDispatcher rd = request.getRequestDispatcher("Page1");  
	        rd.forward(request, response);  
		}
		else if(value<99)
		{
			RequestDispatcher rd = request.getRequestDispatcher("Page2");  
	        rd.forward(request, response);
		}
		else
		{
			RequestDispatcher rd = request.getRequestDispatcher("Page3");  
	        rd.forward(request, response);
		}
	}

}
