import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Page1")
public class Page1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print(
				"<html>"+
					"<title>Page 1</title>"+
					"<body>"+
						"<h1>Welcome to Page 1</h1>"+
						"<p>Input number is: "+request.getParameter("input")+"</p>"+
					"</body>"+
				"</html>"
				);
		out.close();
	}

}
