import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		ServletConfig config = getServletConfig();
		ServletContext context = config.getServletContext();
				
		String className = context.getInitParameter("className");
		String url = context.getInitParameter("url");
		String user = context.getInitParameter("user");
		String password = context.getInitParameter("password");
		
		out.print(
				"<html>"+
						"<title>To read Database connection information from web.xml</title>"+
						"<body>"+
							"<h1>To read Database connection information from web.xml</h1><br>"+
							"<p>ClassName: "+className+"</p>"+
							"<p>URL: "+url+"</p>"+
							"<p>User: "+user+"</p>"+
							"<p>Passwor: "+password+"</p>"+
						"</body>"+
					"</html>"
				);
		out.close();
		
	}

}
