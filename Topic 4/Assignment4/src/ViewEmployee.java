import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/ViewEmployee")
public class ViewEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter(); 
        
        HttpSession session=request.getSession(false);
        
        if(session!=null){  
            String name = (String)session.getAttribute("userName");  
              
            Connection con=null;  
            try{  
                Class.forName("oracle.jdbc.driver.OracleDriver");  
                con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","hr","hr");
                PreparedStatement ps = con.prepareStatement("select * from EmployeeDetails");  
	            ResultSet rs=ps.executeQuery();
	            out.println("<h1>Hello, "+name+"</h1>");
	            out.println("<h1>Employee List</h1>");
	            out.print("<table border='1' width='100%'");  
	            out.print("<tr><th>Employee Id</th><th>Name</th><th>DOB</th><th>Address</th><th>Business Unit</th></tr>");  
	            
	            while(rs.next()){ 
	            	out.print("<tr>"+
            		 		"<td>"+rs.getInt(1)+"</td>"+
            		 		"<td>"+rs.getString(2)+"</td>"+
            		 		"<td>"+rs.getDate(3)+"</td>"+
            		 		"<td>"+rs.getString(4)+"</td>"+
            		 		"<td>"+rs.getString(5)+"</td>"+
            		 	"</tr>");
	            	out.print("</table>");  
	            	con.close();
	            }
            }
            catch(Exception e)
            {
            	System.out.println(e);
            }
              
            out.close();
            
        }  
        else{  
                out.print("Please login first");  
                request.getRequestDispatcher("login.html").include(request, response);  
        }  
        out.close();  
            
	}

}
