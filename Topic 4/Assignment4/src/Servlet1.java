

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Servlet1
 */
@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("username");  
	    String userPass = request.getParameter("password");
	    
	    Connection con;
		PreparedStatement ps;
		ResultSet rs;
		
	    try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","hr","hr");
	        
	        ps = con.prepareStatement("select * from usertable where username=? and password=?");
	        ps.setString(1, userName);
	        ps.setString(2, userPass);
	        
	        rs = ps.executeQuery();
            
	        response.setContentType("text/html");  
    		PrintWriter out = response.getWriter();
    		
    		if(rs.next())
	        {
    			out.print("<html>");
    			out.print("<title>Welcome</title>");
    			out.print("<body>");
    			out.print("<h1>Welcome "+userName+"</h1><br>");
    	        out.print("<a href=\"ViewEmployee\">Get Employee Details</a>");
    	        out.print("<body>");
    	        out.print("</html>");
    	        HttpSession session = request.getSession();  
    	        session.setAttribute("userName",userName); 
    	        session.setAttribute("userPass",userPass); 
	        }
	        else
	        {
	        	out.print("Sorry Username or Password Error!\n");
		        RequestDispatcher rd=request.getRequestDispatcher("/login.html");  
		        rd.include(request, response);  
		                      
	        }
    		out.close();
    		con.close();
    		
		}
		catch(Exception e)
		{
			System.out.println("\nError: "+e);
			e.printStackTrace();
		}
	    
	}

}
