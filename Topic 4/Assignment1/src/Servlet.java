import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		
		Cookie ck1 = new Cookie("userName", userName);
		Cookie ck2 = new Cookie("password", password);
		
		response.addCookie(ck1);
		response.addCookie(ck2);
		
		RequestDispatcher rd = request.getRequestDispatcher("Validate");
		rd.forward(request, response);
		
	}

}
