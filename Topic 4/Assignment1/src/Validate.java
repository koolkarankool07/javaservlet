import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Validate")
public class Validate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie c[]=request.getCookies(); 
		String userName = c[0].getValue();
		String password = c[1].getValue();
		
		Connection con;
		PreparedStatement ps;
		ResultSet rs;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","hr","hr");

	        ps = con.prepareStatement("select * from usertable where username=? and password=?");
	        ps.setString(1, userName);
	        ps.setString(2, password);
	        
	        rs = ps.executeQuery(); 
	        
	        String result="Not Matched";
    		if(rs.next())
	        {
	        	result="Matched";
	        }
	        
	        response.setContentType("text/html");  
    		PrintWriter out = response.getWriter();
    		
    		out.print(
    				"<html>"+
    					"<title>Validate</title>"+
    					"<body>"+
    						"<h1>Validate</h1>"+
    						"<table border='1' width='100%'"+
    							"<tr><th>Username (Cookie)</th><th>Password (Cookie)</th><th>Validate result</th></tr>"+
    							"<tr><td>"+userName+"</td><td>"+password+"</td><td>"+result+"</td></tr>"+
    						"</table>"+
    					"</body>"+
    				"</html>"
    				);
    		out.close();
    		
    		con.close();

	        
		}
		catch(Exception e)
		{
			System.out.println("\nError: "+e);
			e.printStackTrace();
		}
		
	}

}
