import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class WelcomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		
		String title = "Welcome Back";
		
		if (session.isNew()) {
	         title = "Welcome New User";
	      }
		
		response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
		
	    out.print(
				"<html>"+
					"<title>Welcome</title>"+
					"<body>"+
						"<h1>"+title+"</h1>"+
					"</body>"+
				"</html>"
				);
		out.close();
	}

}
