
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet1")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Connection con;
	PreparedStatement ps;
	ResultSet rs;
	
	public void init() throws ServletException {
		
		System.out.println("Inside Init");
	
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","hr","hr");
	        System.out.println("Connection Established.");
		}
		catch(Exception e)
		{
			System.out.println("\nError: "+e);
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userName = request.getParameter("userName");  
	    String userPass = request.getParameter("userPass"); 
		try
		{
			ps = con.prepareStatement("select * from usertable where username=? and password=?");
	        ps.setString(1, userName);
	        ps.setString(2, userPass);
	        
	        rs = ps.executeQuery();
	        
	        response.setContentType("text/html");  
    		PrintWriter out = response.getWriter();
	        
	        if(rs.next())
	        {
	        	if(rs.getString(3).equals("A"))
	        	{
	        		RequestDispatcher rd=request.getRequestDispatcher("/Admin");  
			        rd.forward(request, response);
	        	}
	        	else
	        	{
	        		RequestDispatcher rd=request.getRequestDispatcher("/Employee");  
			        rd.forward(request, response);
	        	}
	        }
	        else
	        {
	        	out.print("Sorry UserName or Password Error!");
		        RequestDispatcher rd=request.getRequestDispatcher("/index.html");  
		        rd.include(request, response);  
		                      
	        }
		}
		catch(Exception e)
		{
			System.out.println("\nError: "+e);
			e.printStackTrace();
		}
	}
	
	public void destroy() {
			try
			{
				con.close();
				System.out.println("Connection Closed");
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			
	}
}