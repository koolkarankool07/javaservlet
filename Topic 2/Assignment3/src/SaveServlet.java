

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter(); 
        
        int trainingId = Integer.valueOf(request.getParameter("trainingId"));
    	String trainingName = request.getParameter("trainingName");
    	Date startDate = Date.valueOf(request.getParameter("startDate"));
    	Date endDate = Date.valueOf(request.getParameter("endDate"));;
    	String trainingMode = request.getParameter("trainingMode");
    	String businessUnit = request.getParameter("businessUnit");
    	String contactPersonId = request.getParameter("contactPersonId");
    	
    	TrainingDetail training = new TrainingDetail(trainingId, trainingName, startDate, endDate, trainingMode, businessUnit, contactPersonId);
    	
    	int status = TrainingDAO.save(training);
    	
    	 if(status>0)
    	 {  
             out.print("<p>Record saved successfully!</p>");  
             request.getRequestDispatcher("addtraining.html").include(request, response);  
         }
    	 else
    	 {  
             out.println("Sorry! unable to save record");  
         }  
           
         out.close(); 
	}

}
