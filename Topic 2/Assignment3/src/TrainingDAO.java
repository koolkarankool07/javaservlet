import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TrainingDAO {

	public static Connection getConnection(){  
        Connection con=null;  
        try{  
            Class.forName("oracle.jdbc.driver.OracleDriver");  
            con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","hr","hr");  
        }
        catch(Exception e)
        {
        	System.out.println(e);
        }  
        return con;  
    }
	
	 public static int save(TrainingDetail e){  
	        int status=0;  
	        try{  
	            Connection con = TrainingDAO.getConnection();  
	            PreparedStatement ps=con.prepareStatement(  
	            		"insert into TrainingDetail(trainingid,trainingname,startdate,enddate,trainingmode,businessunit,contactpersonid) values (?,?,?,?,?,?,?)");  
	            ps.setInt(1,e.getTrainingId());  
	            ps.setString(2,e.getTrainingName());  
	            ps.setDate(3,e.getStartDate());
	            ps.setDate(4,e.getEndDate());
	            ps.setString(5,e.getTrainingMode());
	            ps.setString(6,e.getBusinessUnit());
	            ps.setString(7,e.getContactPersonId());
	              
	            status=ps.executeUpdate();  
	              
	            con.close();  
	        }
	        catch(Exception ex)
	        {
	        	ex.printStackTrace();
	        }  
	          
	        return status;  
	    } 
	 
	 public static int delete(int id){  
	        int status=0;  
	        try{  
	            Connection con=TrainingDAO.getConnection();  
	            PreparedStatement ps=con.prepareStatement("delete from TrainingDetail where trainingid=?");  
	            ps.setInt(1,id);  
	            status=ps.executeUpdate();  
	              
	            con.close();  
	        }
	        catch(Exception e)
	        {
	        	e.printStackTrace();
	        }  
	          
	        return status;  
	    }  
	 
	 public static List<TrainingDetail> getTrainings(){  
	        List<TrainingDetail> list = new ArrayList<TrainingDetail>();  
	          
	        try{  
	            Connection con = TrainingDAO.getConnection();  
	            PreparedStatement ps = con.prepareStatement("select * from TrainingDetail");  
	            ResultSet rs=ps.executeQuery();  
	            while(rs.next()){  
	                TrainingDetail training = new TrainingDetail(rs.getInt(1),rs.getString(2),rs.getDate(3),rs.getDate(4),rs.getString(5),rs.getString(6),rs.getString(7)); 
	                list.add(training);  
	            }  
	            con.close();  
	        }
	        catch(Exception e)
	        {
	        	e.printStackTrace();
	        }  
	          
	        return list;  
	    }  
	
}
