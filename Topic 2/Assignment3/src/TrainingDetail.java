import java.sql.Date;

public class TrainingDetail {
	private int trainingId;
	private String trainingName;
	private Date startDate;
	private Date endDate;
	private String trainingMode;
	private String businessUnit;
	private String contactPersonId;
	
	
	
	public TrainingDetail(int trainingId, String trainingName, Date startDate, Date endDate, String trainingMode,
			String businessUnit, String contactPersonId) {
		super();
		this.trainingId = trainingId;
		this.trainingName = trainingName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.trainingMode = trainingMode;
		this.businessUnit = businessUnit;
		this.contactPersonId = contactPersonId;
	}
	public TrainingDetail() {
		
	}
	public int getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}
	public String getTrainingName() {
		return trainingName;
	}
	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getTrainingMode() {
		return trainingMode;
	}
	public void setTrainingMode(String trainingMode) {
		this.trainingMode = trainingMode;
	}
	public String getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getContactPersonId() {
		return contactPersonId;
	}
	public void setContactPersonId(String contactPersonId) {
		this.contactPersonId = contactPersonId;
	}
	
	
}
