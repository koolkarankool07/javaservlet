

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        
        out.println("<h1>Training List</h1>");  
        
        List<TrainingDetail> list = TrainingDAO.getTrainings();
        
        out.print("<table border='1' width='100%'");  
        out.print("<tr><th>Training Id</th><th>Training Name</th><th>Start Date</th><th>End Date</th><th>Training Mode</th><th>Business Unit</th><th>Contact Person Id</th><th>Delete</th></tr>");  
        for(TrainingDetail training:list){  
         out.print("<tr>"+
        		 		"<td>"+training.getTrainingId()+"</td>"+
        		 		"<td>"+training.getTrainingName()+"</td>"+
        		 		"<td>"+training.getStartDate()+"</td>"+
        		 		"<td>"+training.getEndDate()+"</td>"+
        		 		"<td>"+training.getTrainingMode()+"</td>"+
        		 		"<td>"+training.getBusinessUnit()+"</td>"+
        		 		"<td>"+training.getContactPersonId()+"</td>"+
        		 		"<td><a href='DeleteServlet?trainingId="+training.getTrainingId()+"'>Delete</a></td>"+
        		 	"</tr>");  
        }  
        out.print("</table>");  
          
        out.close();  
	}

}
